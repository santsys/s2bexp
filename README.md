# README #

Simple Export utility designed to help pull flat file information from SQL databases. This code contains custom functions specific to the Aeries SIS system.

# Options #

```
#!text

        Options     | Example                | Description
---------------------------------------------------------------------------------------------------------------------------
        -?, -h, -v  |                        | Show this help message.
        -q          | -q "SELECT * FROM ..." | Specify an inline query to be run.
        -f          | -f "C:\query.sql"      | Specify an file containing an SQL Query to be run.
        -s          | -s "SERVER NAME"       | Specify the SQL Server name.
        -i          |                        | Use a trusted connection to SQL Server (no user/password).
        -u          | -u "USER NAME"         | Supply a user name to the connection.
        -p          | -p "PASSWORD"          | Supply a password to the connection
        -db         | -db "DST14000TEST"     | Specify the inital catalog (database) to use.
        -dbac       | -dbac "07/01/Aeries"   | Set the database to the current Aeries database.
                    |                        | Use "MM/DD/Prefix" to define your rollover date and your database prefix.
        -o          | -o "C:\output.csv"     | Specify the path and file to save the exported data.
        -d          | -d ","                 | Specify the delimiter to use between columns.
        -w          |                        | Wrap text columns in quotation marks (").
        -wa         |                        | Wrap all columns in quotation marks (").
        -nh         |                        | Exclude the column header row from the extracted data.
        -pic        |                        | Aeries PIC table export (saves pictures with as STU.PID)
        -clean      |                        | 'Pretty' output when printing to the console.

Example:

        s2bexp.exe -q "SELECT ID, SC, SN FROM STU WHERE DEL = 0" -s "sql.local" -i -db "DST14000TEST"
        s2bexp.exe -q "SELECT ID, SC, SN FROM STU WHERE DEL = 0" -s "sql.local" -u user -p passwd -db "DST14000TEST" -clean
        s2bexp.exe -f "c:\mysql.sql" -s "sql.local" -u user -p passwd -db "DST14000TEST" -o "out.csv"
        s2bexp.exe -pic -s "sql.local" -u user -p passwd -db "DST14000TEST" -o "c:\Pictures\"
        s2bexp.exe -pic -s "sql.local" -u user -p passwd -dbac "09/01/Aeries" -o "c:\Pictures\"

```
