﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace s2bexp
{
	class Program
	{
		//static string _logfile = "last_run.log";

		private static void ShowHeader()
		{
			int width = Console.WindowWidth;

			Console.Write("*".PadRight(width, '*'));

			Console.WriteLine("Santomieri Systems");
			Console.WriteLine("Simple Bulk Export Tool (s2bexp.exe) for SQL Server.");
			Console.WriteLine("http://www.santsys.com/");
			Console.WriteLine("");

			Assembly aVer = Assembly.GetExecutingAssembly();
			if (aVer != null) {
				AssemblyName verName = aVer.GetName();
				if (verName != null) {
					Console.WriteLine(string.Format("Version: {0}.{1}.{2}", verName.Version.Major, verName.Version.Minor, verName.Version.Revision));
					FileInfo aInfo = new FileInfo(aVer.Location);
					if (aInfo != null) {
						Console.WriteLine(string.Format("{0} {1}", "Date:".PadLeft(8, ' '), aInfo.LastWriteTime.ToShortDateString()));
					}
				}
			}

			Console.Write("*".PadRight(width, '*'));
		}

		private static void ShowHelp()
		{
			int width = Console.WindowWidth;

			Console.WriteLine("Options:\r\n");

			// write out the base options
			List<string[]> options = new List<string[]>();

			options.Add(new string[] { "-?, -h, -v", "", "Show this help message." });
			options.Add(new string[] { "-q", "-q \"SELECT * FROM ...\"", "Specify an inline query to be run." });
			options.Add(new string[] { "-f", "-f \"C:\\query.sql\"", "Specify an file containing an SQL Query to be run." });
			options.Add(new string[] { "-s", "-s \"SERVER NAME\"", "Specify the SQL Server name." });
			options.Add(new string[] { "-i", "", "Use a trusted connection to SQL Server (no user/password)." });
			options.Add(new string[] { "-u", "-u \"USER NAME\"", "Supply a user name to the connection." });
			options.Add(new string[] { "-p", "-p \"PASSWORD\"", "Supply a password to the connection" });
			options.Add(new string[] { "-db", "-db \"DST14000TEST\"", "Specify the inital catalog (database) to use." });
			options.Add(new string[] { "-dbac", "-dbac \"07/01/Aeries\"", "Set the database to the current Aeries database.\r\nUse \"MM/DD/Prefix\" to define your rollover date and your database prefix." });
			options.Add(new string[] { "-o", "-o \"C:\\output.csv\"", "Specify the path and file to save the exported data." });
			options.Add(new string[] { "-d", "-d \",\"", "Specify the delimiter to use between columns." });
			options.Add(new string[] { "-w", "", "Wrap text columns in quotation marks (\")." });
			options.Add(new string[] { "-wa", "", "Wrap all columns in quotation marks (\")." });
			options.Add(new string[] { "-nh", "", "Exclude the column header row from the extracted data." });
			options.Add(new string[] { "-pic", "", "Aeries PIC table export (saves pictures with as STU.PID)" });
			options.Add(new string[] { "-clean", "", "'Pretty' output when printing to the console." });


			int oLen = (from s in options select s[0].Length).Max();
			int eLen = (from s in options select s[1].Length).Max();

			if (oLen < 7) oLen = 7;
			if (eLen < 7) eLen = 7;

			Console.WriteLine(string.Format("\t{0} | {1} | {2}", "Options".PadRight(oLen + 1, ' '), "Example".PadRight(eLen, ' '), "Description"));
			Console.Write("-".PadRight(width, '-'));

			StringBuilder desc = new StringBuilder();
			foreach (string[] o in options) {
				desc.Clear();
				if (o[2].Contains("\r\n")) {
					string[] descParts = o[2].Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
					foreach (string p in descParts) {
						if (desc.Length > 0) {
							desc.Append("\r\n");
							desc.Append(string.Format("\t{0} | {1} | {2}", "".PadRight(oLen + 1, ' '), "".PadRight(eLen, ' '), p));
						}
						else {
							desc.Append(p);
						}
					}
				}
				else {
					desc.Append(o[2]);
				}

				Console.WriteLine(string.Format("\t{0} | {1} | {2}", o[0].PadRight(oLen + 1, ' '), o[1].PadRight(eLen, ' '), desc.ToString()));
			}

			Console.WriteLine("");

			Console.WriteLine("Example:\r\n");
			Console.WriteLine("\ts2bexp.exe -q \"SELECT ID, SC, SN FROM STU WHERE DEL = 0\" -s \"sql.local\" -i -db \"DST14000TEST\"");
			Console.WriteLine("\ts2bexp.exe -q \"SELECT ID, SC, SN FROM STU WHERE DEL = 0\" -s \"sql.local\" -u user -p passwd -db \"DST14000TEST\" -clean");
			Console.WriteLine("\ts2bexp.exe -f \"c:\\mysql.sql\" -s \"sql.local\" -u user -p passwd -db \"DST14000TEST\" -o \"out.csv\"");
			Console.WriteLine("\ts2bexp.exe -pic -s \"sql.local\" -u user -p passwd -db \"DST14000TEST\" -o \"c:\\Pictures\\\"");
			Console.WriteLine("\ts2bexp.exe -pic -s \"sql.local\" -u user -p passwd -dbac \"09/01/Aeries\" -o \"c:\\Pictures\\\"");
			
			Console.WriteLine("");
		}

		private static bool ProcessArgs(string[] args, ref BulkExporter exp)
		{
			if (args == null || args.Length < 1) {
				return false;
			}

			int argCount = args.Length;

			for (int i = 0; i < argCount; i++) {
				string v = args[i];

				// if the user fat-fingered an extra dash, lets just remove it
				if (v.StartsWith("--")) {
					v = v.Substring(1);
				}

				if (string.Compare("-?", v, true) == 0
					|| string.Compare("-h", v, true) == 0
					|| string.Compare("-v", v, true) == 0) {
					return false;
				}
				else if (string.Compare("-q", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					if (!string.IsNullOrEmpty(exp.FileQuery)) {
						Console.WriteLine("Error: You can not specify an inline query and a file query at the same time.");
						return false;
					}
					string query = args[i + 1];
					if (query.StartsWith("-")) continue;
					exp.Query = query;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-f", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					if (!string.IsNullOrEmpty(exp.Query)) {
						Console.WriteLine("Error: You can not specify an inline query and a file query at the same time.");
						return false;
					}
					string filePath = args[i + 1];
					if (filePath.StartsWith("-")) continue;
					exp.FileQuery = filePath;

					if (!File.Exists(filePath)) {
						Console.WriteLine("Error: The file specified does not exist.");
						return false;
					}

					i++; // skip ahead one argument
				}
				else if (string.Compare("-s", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string server = args[i + 1];
					if (server.StartsWith("-")) continue;
					exp.ServerName = server;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-i", v, true) == 0) {
					exp.UseTrustedConnection = true;
				}
				else if (string.Compare("-u", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string user = args[i + 1];
					if (user.StartsWith("-")) continue;
					exp.UserName = user;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-p", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string pwd = args[i + 1];
					if (pwd.StartsWith("-")) continue;
					exp.Password = pwd;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-db", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string ic = args[i + 1];
					if (ic.StartsWith("-")) continue;
					exp.Database = ic;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-dbac", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string rolloverDate = args[i + 1];
					if (rolloverDate.StartsWith("-")) continue;
					i++; // skip ahead one argument

					string[] dateParts = rolloverDate.Split('/');

					if (dateParts.Length > 2) {

						int month = 0;
						int day = 0;

						if (!int.TryParse(dateParts[0], out month)) {
							continue;
						}

						if (!int.TryParse(dateParts[1], out day)) {
							continue;
						}

						DateTime today = DateTime.Now;
						DateTime roll = new DateTime(today.Year, month, day);

						if (roll > today) {
							exp.Database = string.Format("DST{0}000{1}", (today.Year - 1).ToString().Substring(2, 2), dateParts[2]);
						}
						else {
							exp.Database = string.Format("DST{0}000{1}", (today.Year).ToString().Substring(2, 2), dateParts[2]);
						}					
					}
					else {
						continue;
					}
				}
				else if (string.Compare("-o", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string fOut = args[i + 1];
					if (fOut.StartsWith("-")) continue;
					exp.ExportTo = fOut;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-d", v, true) == 0) {
					if ((i + 1) >= argCount) {
						Console.WriteLine("Error: Invalid number of arguments.");
						return false;
					}
					string del = args[i + 1];
					if (del.StartsWith("-")) continue;
					exp.Delimiter = del;
					i++; // skip ahead one argument
				}
				else if (string.Compare("-w", v, true) == 0) {
					exp.QuoteStrings = true;
				}
				else if (string.Compare("-wa", v, true) == 0) {
					exp.QuoteAll = true;
				}
				else if (string.Compare("-nh", v, true) == 0) {
					exp.IncludeHeader = false;
				}
				else if (string.Compare("-pic", v, true) == 0) {
					exp.DoPicExport = true;
				}
				else if (string.Compare("-clean", v, true) == 0) {
					exp.CleanOutput = true;
				}
			}

			if (string.IsNullOrEmpty(exp.ServerName)) {
				Console.WriteLine("Error: You must specify a server to connect to.");
				return false;
			}

			return true;
		}

		static void Main(string[] args)
		{
			try {
				try {
					if (Console.WindowWidth < 130) {
						Console.WindowWidth = 130;
						Console.BufferWidth = 130;
					}
				}
				catch (Exception ex) {
					Console.WriteLine("Unable to resize the console, we recommend a width of 130 or greater.\r\n" + (string.IsNullOrWhiteSpace(ex.Message) ? "" : ex.Message + "\r\n"));
				}

				ShowHeader();

				BulkExporter exp = BulkExporter.Create();

				// so we can write to the console if there is not output file specified
				exp.OutputStream = Console.OpenStandardOutput();

				if (!ProcessArgs(args, ref exp)) {
					ShowHelp();
					return;
				}

				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				
				int records = exp.Run();
				
				sw.Stop();

				Console.WriteLine(string.Format("{0:#,##0} record(s).", records));
				TimeSpan ts = sw.Elapsed;
				if(ts.TotalMilliseconds >= 0.009d) {
					Console.WriteLine(string.Format("{0:00}:{1:00}:{2:00}.{3:00} time taken.", ts.Hours, ts.Minutes, ts.Seconds, (ts.Milliseconds / 10)));
				}


#if DEBUG
				Console.Read();
#endif
			}
			catch (Exception ex) {
				Console.WriteLine("Error: " + ex.Message);
				Console.WriteLine(ex.StackTrace);

#if DEBUG
				Console.Read();
#endif
			}
		}
	}
}
