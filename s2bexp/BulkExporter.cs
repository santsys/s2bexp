﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace s2bexp
{
	public class BulkExporter
	{
		public string ExportTo { get; set; }
		public string ServerName { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Database { get; set; }
		public bool UseTrustedConnection { get; set; }
		public string ConnectionString { get; set; }
		public bool IncludeHeader { get; set; }
		public bool QuoteAll { get; set; }
		public bool QuoteStrings { get; set; }
		public string Query { get; set; }
		public string FileQuery { get; set; }
		public string Delimiter { get; set; }
		public bool DoPicExport { get; set; }
		public Stream OutputStream { get; set; }
		public bool CleanOutput { get; set; }

		BulkExporter()
		{
			this.UseTrustedConnection = false;
			this.IncludeHeader = true;
			this.Delimiter = ",";
			this.QuoteAll = false;
			this.QuoteStrings = false;
			this.DoPicExport = false;
			this.CleanOutput = false;
		}

		internal string GetConnectionString()
		{
			SqlConnectionStringBuilder cBuilder = new SqlConnectionStringBuilder();
			cBuilder.DataSource = this.ServerName;
			cBuilder.ConnectTimeout = 10;
			cBuilder.IntegratedSecurity = this.UseTrustedConnection;
			if (!cBuilder.IntegratedSecurity) {
				cBuilder.UserID = this.UserName;
				cBuilder.Password = this.Password;
			}
			cBuilder.InitialCatalog = this.Database;
			return cBuilder.ConnectionString;
		}

		internal string GetSql()
		{
			if (this.DoPicExport) {
				return "SELECT PIC.ID, PIC.RB, PIC.XT FROM PIC INNER JOIN (SELECT ID, MAX(YR) AS YR FROM PIC WHERE PIC.DEL = 0 AND PIC.RB IS NOT NULL GROUP BY ID) AS PIC1 ON PIC.ID = PIC1.ID WHERE PIC.DEL = 0 AND PIC.YR = PIC1.YR AND PIC.RB IS NOT NULL";
			}
			else {
				if (string.IsNullOrWhiteSpace(this.Query)) {
					if (!string.IsNullOrWhiteSpace(this.FileQuery)) {
						string query = File.ReadAllText(this.FileQuery);
						return query;
					}
					return null;
				}
				return this.Query;
			}
		}

		public int Run()
		{
			string sql = GetSql();
			if (string.IsNullOrWhiteSpace(sql)) {
				throw new Exception("No query to run. Please provide a query.");
			}

			int records = 0;

			string strConn = GetConnectionString();
			using (SqlConnection con = new SqlConnection(strConn)) {
				con.Open();

				using (SqlCommand cmd = new SqlCommand(sql, con)) {
					if (BulkExporter.IsSelect(sql)) {
						using (SqlDataReader reader = cmd.ExecuteReader()) {
							DataTable data = new DataTable();
							data.Load(reader);
							if (data != null && data.Rows != null && data.Rows.Count > 0) {
								if (this.DoPicExport) {
									this.ExportPictures(data);
								}
								else {
									this.ExtractData(data);
								}
								records = data.Rows.Count;
							}
						}
					}
					else {
						records = cmd.ExecuteNonQuery();
					}
				}

				con.Close();
			}

			return records;
		}

		internal void ExportPictures(DataTable data)
		{
			if (data == null) { return; }

			string path = Path.GetFullPath(this.ExportTo);

			if (!Directory.Exists(path)) {
				Directory.CreateDirectory(path);
			}

			foreach (DataRow r in data.Rows) {
				string file = Path.Combine(path, r["ID"].ToString() + "." + r["XT"].ToString());
				if (File.Exists(file)) {
					File.Delete(file);
				}
				using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate)) {
					using (BinaryWriter bWrite = new BinaryWriter(fs)) {
						bWrite.Write(r.Field<byte[]>("RB"));
						bWrite.Close();
					}
					fs.Close();
				}
			}
		}

		internal void ExtractData(DataTable data)
		{
			bool doConsole = false;

			if (data == null) { return; }

			if (string.IsNullOrWhiteSpace(this.ExportTo) && this.OutputStream == null) { return; }

			Stream outStream = null;
			if (!string.IsNullOrWhiteSpace(this.ExportTo)) {
				if (File.Exists(this.ExportTo)) {
					File.Delete(this.ExportTo);
				}
				outStream = new FileStream(this.ExportTo, FileMode.Create);
			}
			else {
				outStream = this.OutputStream;
				doConsole = true;
			}

			// doubly check we have some place to write
			if (outStream == null) { return; }

			// if we are doing clean console output, calculate column lengths for spacing
			Dictionary<DataColumn, int> colWidths = new Dictionary<DataColumn, int>();
			int overallLen = 1;
			if (doConsole && this.CleanOutput) {
				foreach (DataColumn c in data.Columns) {
					int max = 0;

					// if we have a date time, make sure we have times, otherwise use short date format
					if (c.DataType == typeof(DateTime)) {
						int datesWithTime = (from r in data.AsEnumerable() select r.Field<DateTime>(c)).Where(x => x.TimeOfDay != TimeSpan.Zero).Count();
						if(datesWithTime > 0) {
							max = (from r in data.AsEnumerable() select r.Field<object>(c).ToString().Length).Max();
						}
						else {
							max = 10;
						}
					}
					else {
						max = (from r in data.AsEnumerable() select r.Field<object>(c).ToString().Length).Max();
					}

					max = max > c.ColumnName.Length ? max : c.ColumnName.Length;
					overallLen += max + 2 + 1;
					colWidths.Add(c, max);
				}
			}

			// UTF8 without BOM
			Encoding outputEncoding = new UTF8Encoding(false);

			using (StreamWriter sw = new StreamWriter(outStream, outputEncoding, 1024)) {
				StringBuilder s = new StringBuilder();

				// line for formatting
				if (doConsole) {
					sw.WriteLine("");
				}

				// print out the query if we are doing console with clean output
				if (doConsole && this.CleanOutput) {
					sw.WriteLine(" Database: {0}", this.Database);
					sw.WriteLine("SQL Query: {0}", this.GetSql());
					sw.WriteLine("");
				}

				// create and write the header
				if (this.IncludeHeader) {
					s.Clear();
					foreach (DataColumn c in data.Columns) {
						if (s.Length > 0 || (doConsole && this.CleanOutput)) {
							if (doConsole && this.CleanOutput) {
								s.Append("|");
							}
							else {
								s.Append(this.Delimiter);
							}
						}

						if (doConsole && this.CleanOutput) {
							if (c.DataType == typeof(int)
								|| c.DataType == typeof(long)
								|| c.DataType == typeof(short)
								|| c.DataType == typeof(byte)
								|| c.DataType == typeof(float)
								|| c.DataType == typeof(decimal)) {
								s.Append(c.ColumnName.PadLeft(colWidths[c] + 1) + " ");
							}
							else {
								s.Append(" " + c.ColumnName.PadRight(colWidths[c] + 1));
							}
						}
						else {
							if (this.QuoteStrings || this.QuoteAll) {
								s.Append(string.Format("\"{0}\"", c.ColumnName));
							}
							else {
								s.Append(c.ColumnName);
							}
						}
					}

					// add ending marker
					if (doConsole && this.CleanOutput) {
						s.Append("|");
					}

					sw.WriteLine(s.ToString());

					if (doConsole && this.CleanOutput) {
						s.Clear();
						sw.WriteLine("-".PadRight(overallLen, '-'));
					}
				}

				long row = 0;

				// loop through the rows
				foreach (DataRow r in data.Rows) {
					s.Clear();

					// move to the next line
					if (row > 0) {
						sw.Write("\r\n");
					}

					// begining row "box"
					if (doConsole && this.CleanOutput) {
						s.Append("|");
					}

					foreach (DataColumn c in data.Columns) {
						object o = null;

						o = r[c];
						
						if (doConsole && this.CleanOutput) {
							if (o != null) {
								
								// print numbers to the right
								if(c.DataType == typeof(int)
									|| c.DataType == typeof(long)
									|| c.DataType == typeof(short)
									|| c.DataType == typeof(byte)
									|| c.DataType == typeof(decimal)
									|| c.DataType == typeof(float)) {
									s.Append(o.ToString().PadLeft(colWidths[c] + 1) + " ");
								}
								else if (c.DataType == typeof(DateTime) && o is DateTime) {
									DateTime date = (DateTime)o;
									// if a date has no time, just print the date.
									if (date.TimeOfDay == TimeSpan.Zero) {
										s.Append(date.ToString("yyyy-MM-dd").PadLeft(colWidths[c] + 1) + " ");
									}
									else {
										s.Append(date.ToString().PadLeft(colWidths[c] + 1) + " ");
									}
								}
								else {
									// print everything else to the left
									s.Append(" " + o.ToString().PadRight(colWidths[c] + 1));
								}
							}
							else {
								// if o is null, print blank
								s.Append(" " + o.ToString().PadRight(colWidths[c] + 1));
							}
						}

						if (s.Length > 0) {
							if (doConsole && this.CleanOutput) {
								s.Append("|");
							}
							else {
								s.Append(this.Delimiter);
							}
						}

						// only so this stuff for file and "dirty" console ouput
						if (!doConsole || (doConsole && !this.CleanOutput)) {
							if (this.QuoteAll) {
								s.Append(string.Format("\"{0}\"", Convert.ToString(o)));
							}
							else if (this.QuoteStrings) {
								if (c.DataType == typeof(string)) {
									s.Append(string.Format("\"{0}\"", Convert.ToString(o)));
								}
								else {
									s.Append(o);
								}
							}
							else {
								s.Append(o);
							}
						}
					}

					sw.Write(s.ToString());
					row++;
				}

				// extra line for console formatting 
				if (doConsole) {
					sw.WriteLine("\r\n");
				}

				sw.Close();
			}
		}

		internal string FormatColumn(object value)
		{
			return null;
		}

		public static BulkExporter Create()
		{
			BulkExporter b = new BulkExporter();
			return b;
		}

		public static bool IsSelect(string sql)
		{
			if (string.IsNullOrWhiteSpace(sql)) {
				return false;
			}
			string uSql = sql.ToUpper();
			return (uSql.Contains(" SELECT") || uSql.Contains("SELECT ") || uSql.Contains("SELECT\r\n") || uSql.Contains("SELECT\n") || uSql.Contains("SELECT\t"));
		}

	}
}
